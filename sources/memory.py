﻿import time, pygame
from pygame.locals import *
from random import *
import sys
from PIL import Image

def creer_matrice(h,l,v):
        """
        Création d'une matrice
        h: hauteur
        l: largeur
        v: valeur
        exemple :
        >>> creer_matrice(2,3,4)
        [[4, 4, 4], [4, 4, 4]]
        """
        t = [[v]*l for i in range(h)]
        return t

# ------------- FONCTIONS --------------
def memory(window_size, debut):

    pygame.init()

    test = None

    # Définissez la taille de la fenêtre en fonction de la taille de l'écran
    fenetre = pygame.display.set_mode(window_size, pygame.FULLSCREEN)
    deltax = window_size[0]/4 # largeur d'une case
    deltay = window_size[1]/4 # largeur d'une case

    pygame.display.set_caption("Jeu de Mémory")

    # Chargement de l'image du fond
    img = Image.open('grille.png')
    img_resize = img.resize(window_size)
    img_resize.save('grille1.png')

    img2 = pygame.image.load('grille1.png')
    imgrect = img2.get_rect()
    imgrect.center = (window_size[0]/2, window_size[1]/2)

    # Chargement des 8 images
    ima = Image.open('num11.png')
    ima_resize = ima.resize((int(window_size[0]/4),int(window_size[1]/4)))
    ima_resize.save('num1.png')
    im1 = pygame.image.load('num1.png')

    imb = Image.open('num22.png')
    imb_resize = imb.resize((int(window_size[0]/4),int(window_size[1]/4)))
    imb_resize.save('num2.png')
    im2 = pygame.image.load('num2.png')

    imc = Image.open('num33.png')
    imc_resize = imc.resize((int(window_size[0]/4),int(window_size[1]/4)))
    imc_resize.save('num3.png')
    im3 = pygame.image.load('num3.png')

    imd = Image.open('num44.png')
    imd_resize = imd.resize((int(window_size[0]/4),int(window_size[1]/4)))
    imd_resize.save('num4.png')
    im4 = pygame.image.load('num4.png')

    ime = Image.open('num55.png')
    ime_resize = ime.resize((int(window_size[0]/4),int(window_size[1]/4)))
    ime_resize.save('num5.png')
    im5 = pygame.image.load('num5.png')

    imf = Image.open('num66.png')
    imf_resize = imf.resize((int(window_size[0]/4),int(window_size[1]/4)))
    imf_resize.save('num6.png')
    im6 = pygame.image.load('num6.png')

    img = Image.open('num77.png')
    img_resize = img.resize((int(window_size[0]/4),int(window_size[1]/4)))
    img_resize.save('num7.png')
    im7 = pygame.image.load('num7.png')

    imh = Image.open('num88.png')
    imh_resize = imh.resize((int(window_size[0]/4),int(window_size[1]/4)))
    imh_resize.save('num8.png')
    im8 = pygame.image.load('num8.png')

    bravo = Image.open('bravo.jpg')
    bravo_resize = bravo.resize((window_size))
    bravo_resize.save('bravo1.jpg')
    bravo = pygame.image.load('bravo1.jpg')


    # Création d'une liste de 16 cartes
    # Chaque carte est définie par son image, son numéro d'image, son statut (None : cachée ou (col,ligne) : retournée)
    cartes=[[im1,1,None],[im2,2,None],[im3,3,None],[im4,4,None],[im5,5,None],[im6,6,None],[im7,7,None],[im8,8,None],
    [im1,1,None],[im2,2,None],[im3,3,None],[im4,4,None],[im5,5,None],[im6,6,None],[im7,7,None],[im8,8,None]]
    # Mélange des cartes
    shuffle(cartes)

    # Initialisation des cases retournées
    # case1 et case2 sont des booleens
    case1,case2 = False,False

    # Affichage et début du jeux
    continuer1 = True
    while continuer1 == True:

        # placement du fond
        fenetre.blit(img2, imgrect)

        # affichage des cartes retournées
        if case1 : #première carte retournée
            fenetre.blit(cartes[n1][0],(colonne1*deltax,ligne1*deltay))
        if case2 : #dexième carte retournée
            fenetre.blit(cartes[n2][0],(colonne2*deltax,ligne2*deltay))

        # Si deux cartes retournées
        if case1 and case2:
            # si les cartes sont identiques on change leur statut
            if cartes[n1][1] == cartes[n2][1]:
                cartes[n1][2] = (colonne1*deltax,ligne1*deltay)
                cartes[n2][2] = (colonne2*deltax,ligne2*deltay)
            case1,case2 = False,False

        # affichage des paires déjà trouvées
        for carte in cartes:
            if carte[2] != None:
                fenetre.blit(carte[0],carte[2])

        # Temporisation (en seconde)
        time.sleep(0.1)

        pygame.display.update()

        if cartes[0][2] != None and cartes[1][2] != None and cartes[2][2] != None and cartes[3][2] != None and cartes[4][2] != None and cartes[5][2] != None and cartes[6][2] != None and cartes[7][2] != None :

            bravorect = bravo.get_rect()
            bravorect.center = (window_size[0]/2, window_size[1]/2)
            fenetre.blit(bravo, bravorect)
            pygame.display.update()
            time.sleep(3)
            debut = 0 #Pour stop l'alarme
            continuer1 = False


        # Gestion des événements
        for event in pygame.event.get():

            # la touche escape permet de quitter le jeux
            if event.type == KEYDOWN and event.key == K_ESCAPE:
                debut = 0
                continuer1 = False

            # la souris permet de sélectionner une carte
            if event.type == MOUSEBUTTONDOWN and event.button == 1:

                coordonnees = pygame.mouse.get_pos()
                y,x = coordonnees[0],coordonnees[1]

                # Premiere carte à retourner
                if case1 == False:

                    ligne1,colonne1 = x//deltay,y//deltax
                    n1=int(ligne1*4 + colonne1)

                    for i in range (1, 5):
                        for j in range (1, 5):
                            if window_size[0]/4*(i-1) <= y <= window_size[0]/4*i and window_size[1]/4*(j-1) <= x <= window_size[1]/4*j :
                                test = (window_size[0]/4*(i-1), window_size[0]/4*i, window_size[1]/4*(j-1), window_size[1]/4*j)
                                break

                    case1 = True
                    y, x = -1, -1

                # Deuxième carte à retourner
                elif case1 == True :

                    if test[0] <= y <= test[1] and test[2] <= x <= test[3]: #Vérification que c'est une case différente
                        case1 = False

                    else :
                        ligne2,colonne2 = x//deltay,y//deltax
                        n2=int(ligne2*4 + colonne2)
                        case2=True
    return debut

