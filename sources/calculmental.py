#Importation modules, création de la fenêtre graphique et de couleurs
import pygame
from pygame.locals import *
import math
import time
from random import randint
import sys
from PIL import Image
pygame.init()
fenetre_info = pygame.display.Info()
fenetre_longueur, fenetre_hauteur = fenetre_info.current_w, fenetre_info.current_h
window_size = (fenetre_longueur, fenetre_hauteur)
fenetre = pygame.display.set_mode(window_size, pygame.FULLSCREEN)
gris= 127,127,127
blanc = (255, 255, 255)
noir = (0,0,0)
rouge = (255,0,0)
vert = (0,255,0)
bleu = (0,0,255)
police1 = pygame.font.SysFont("Arial", int(fenetre_longueur/10))
police2 = pygame.font.SysFont("Arial", int(fenetre_longueur/6))

# Fin modules et fenetre

# Fonctions



def resolution(calcul, result, solved, solved_rendu, solved_rect, debut, backtoback, backtobackrect):
    '''Cette fonction ne prenant aucun argument dure jusqu'à ce que l'utilisateur ait répondu au calcul
    (juste, retourne 1 ; faux, retourne 0)'''
    finish = False #booléen qui va se valider quand l'utilisateur aura validé une réponse
    text = ''
    while not finish:
        fenetre.blit(backtoback, backtobackrect)
        fenetre.blit(solved_rendu, solved_rect)
        texte1_rendu = police2.render(calcul, True, noir) #affichage des deux textes par dessus le reste
        texte1_rect = texte1_rendu.get_rect(center = (fenetre_longueur/100*50, fenetre_hauteur/100*45))
        textrendu = police2.render(text, True, noir)
        textrect = textrendu.get_rect(center = (fenetre_longueur/100*50, fenetre_hauteur/100*70))
        fenetre.blit(texte1_rendu, texte1_rect)
        fenetre.blit(textrendu, textrect)
        pygame.display.update() #on affiche le tout, ainsi en permanence l'écran est à jour et affiche ce que l'utilisateur a écrit.

        for event in pygame.event.get():
            if event.type == KEYDOWN:
                if event.key == K_ESCAPE:
                    return (solved, 0) #On stoppe la fonction ici donc la boucle while dans exec va s'interrompre et on revient instantanément au menu principal
                elif event.key == K_BACKSPACE:
                    if len(text) > 0:
                        text = text[:-1]
                elif event.key == 13: #13 = touche ENTRÉE
                    if text.strip(' ') == result:
                        succes = 'Correct'
                        succesrendu = police1.render(succes, True, vert) #affichage de Correct
                        succesrect = succesrendu.get_rect(center = (fenetre_longueur/100*50, fenetre_hauteur/100*90))
                        fenetre.blit(succesrendu,succesrect)
                        pygame.display.update()
                        solved += 1
                        time.sleep(1) #on laisse le texte affiché une seconde
                        finish = True #calcul répondu : on ferme la fonction
                    else:
                        succes = 'Incorrect'
                        succesrendu = police1.render(succes, True, rouge) #affichage de incorrect
                        succesrect = succesrendu.get_rect(center = (fenetre_longueur/100*50, fenetre_hauteur/100*90))
                        fenetre.blit(succesrendu,succesrect)
                        pygame.display.update()
                        time.sleep(1)
                        finish = True #le joueur a tort donc on passe au calcul suivant sans changer solved
                else :
                    text += event.unicode #inscrit dans la variable ce que l'utilisateur écrit au clavier
    return (solved, debut)

def operation() :
    '''Cette fonction génère une opération simple (addition, soustraction ou multiplication) aléatoirement'''
    operation = randint(1,3)
    if operation == 1: #addition
        n1 = randint(2,20)
        n2 = randint(2,20)
        result = str(n1 + n2)
        calcul = str(n1) + " + " + str(n2) +" = ?"
    if operation == 2 : #soustraction
        n1 = randint(2,20)
        n2 = randint(2,20)
        result = str(n1 - n2)
        calcul = str(n1) + " - " + str(n2) +" = ?"
    else: #multiplication (tables de 2 à 12)
        n1 = randint(2,12)
        n2 = randint(2,12)
        result = str(n1 * n2)
        calcul = str(n1) + " x " + str(n2) +" = ?"
    return (calcul, result)


def exec(window_size, debut) :
    '''Pour que l'importation d'un jeu dans le programme principal cette fonction consiste à exécuter le jeu calcul mental.'''
    #C'est ce procédé qui sera exécuté qd le programme principal lance le jeu calcul mental

    pygame.init()

    # Définissez la taille de la fenêtre en fonction de la taille de l'écran
    fenetre = pygame.display.set_mode(window_size, pygame.FULLSCREEN)
    pygame.display.set_caption("Jeu de Calcul")

    #Images
    bravo = Image.open('bravo.jpg')
    bravo_resize = bravo.resize((window_size))
    bravo_resize.save('bravo1.jpg')
    bravo = pygame.image.load('bravo1.jpg')

    back = Image.open('mathback.jpg')
    back_resize = back.resize(window_size)
    back_resize.save('mathback1.jpg')
    backtoback = pygame.image.load('mathback1.jpg')
    backtobackrect = backtoback.get_rect()
    backtobackrect.center = (window_size[0]/2, window_size[1]/2)

    solved = 0 #Aucun calcul résolu

    while solved < 10 and debut == 1: #Si on atteint le score de 10 le jeu est fini, si on annule (echap) et que debut = 0 on ferme aussi.
        fenetre.blit(backtoback, backtobackrect)
        solved_rendu = police1.render(str(solved) + " / 10", True, noir)
        solved_rect = solved_rendu.get_rect(center = (fenetre_longueur/100*50, fenetre_hauteur/100*10))
        fromope = operation() #La fonction renvoie un tuple donc j'en extrais les valeurs des deux variables pour éviter un bug
        calcul, result = fromope[0], fromope[1]
        fromreso = resolution(calcul, result, solved, solved_rendu, solved_rect, debut, backtoback, backtobackrect) #On ajoute le résultat du joueur à solved.
        solved, debut = fromreso[0], fromreso[1]

    if solved == 10: #Si on a appuyé sur échap il faut juste revenir au menu principal et ne pas afficher le bravo
        fenetre.blit(backtoback, backtobackrect)
        solved_rendu = police1.render(str(solved) + " / 10", True, noir)
        solved_rect = solved_rendu.get_rect(center = (fenetre_longueur/100*50, fenetre_hauteur/100*10))
        fenetre.blit(solved_rendu, solved_rect)
        pygame.display.update()
        time.sleep(0.5)
        bravorect = bravo.get_rect()
        bravorect.center = (window_size[0]/2, window_size[1]/2)
        fenetre.blit(bravo, bravorect)
        pygame.display.update()
        time.sleep(3)
        debut = 0

    return debut #Renvoie 0 au programme principal afin d'arrêter le processus et revenir à l'écran principal (choix heure)