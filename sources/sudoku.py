import pygame
from pygame.locals import *
import sys
import datetime
from datetime import *
from pygame import mixer
from PIL import Image
from random import *
import time

def sudoku(window_size, debut):

    pygame.init()

    # création d'une couleur (en RVB)
    couleur_fond = 255, 0, 0
    blanc = (255,255,255)
    noir = (0,0,0)

    pygame.display.set_caption("Jeu de Sudoku")

#---Création d'une grille complète et d'une à compléter
    Grille = [[[[[3],[None]],[[4],[None]]],[[[None],[1]],[[None],[2]]]],[[[[None],[4]],[[None],[3]]],[[[2],[None]],[[1],[None]]]]]
    Grillev = [[[[[3],[2]],[[4],[1]]],[[[4],[1]],[[3],[2]]]],[[[[1],[4]],[[2],[3]]],[[[2],[3]],[[1],[4]]]]]

    fenetre = pygame.display.set_mode(window_size, FULLSCREEN)
    fenetre_longueur = window_size[0]
    fenetre_hauteur = window_size[1]

    police1 = pygame.font.SysFont("Arial", int(fenetre_longueur/10))

    bravo = Image.open('bravo.jpg')
    bravo_resize = bravo.resize((window_size))
    bravo_resize.save('bravo1.jpg')
    bravo = pygame.image.load('bravo1.jpg')

    text1 = 0
    text2 = 0
    text3 = 0
    text4 = 0
    text5 = 0
    text6 = 0
    text7 = 0
    text8 = 0

    text11 = 0
    text22 = 0
    text33 = 0
    text44 = 0
    text55 = 0
    text66 = 0
    text77 = 0
    text88 = 0

    continuer = True
    while continuer:

        #Affichage de la grille

        fenetre.fill(couleur_fond)

        pygame.draw.rect(fenetre, blanc, pygame.Rect(fenetre_longueur/100*0, fenetre_hauteur/100*25, fenetre_longueur/100*100, fenetre_hauteur/100*
        1))

        pygame.draw.rect(fenetre, blanc, pygame.Rect(fenetre_longueur/100*0, fenetre_hauteur/100*50, fenetre_longueur/100*100, fenetre_hauteur/100*
        3))

        pygame.draw.rect(fenetre, blanc, pygame.Rect(fenetre_longueur/100*0, fenetre_hauteur/100*75, fenetre_longueur/100*100, fenetre_hauteur/100*
        1))

        pygame.draw.rect(fenetre, blanc, pygame.Rect(fenetre_longueur/100*25, fenetre_hauteur/100*0, fenetre_longueur/100*1, fenetre_hauteur/100*
        100))

        pygame.draw.rect(fenetre, blanc, pygame.Rect(fenetre_longueur/100*50, fenetre_hauteur/100*0, fenetre_longueur/100*3, fenetre_hauteur/100*
        100))

        pygame.draw.rect(fenetre, blanc, pygame.Rect(fenetre_longueur/100*75, fenetre_hauteur/100*0, fenetre_longueur/100*1, fenetre_hauteur/100*
        100))

        #Affichage des valeurs dans la grille
        texte1 = str(Grille[0][0][0][0][0])
        texte1_rendu = police1.render(texte1, True, noir)
        texte1_rect = texte1_rendu.get_rect(center = (fenetre_longueur/100*10, fenetre_hauteur/100*10))
        fenetre.blit(texte1_rendu, texte1_rect)

        texte2 = str(Grille[0][0][1][0][0])
        texte2_rendu = police1.render(texte2, True, noir)
        texte2_rect = texte2_rendu.get_rect(center = (fenetre_longueur/100*60, fenetre_hauteur/100*10))
        fenetre.blit(texte2_rendu, texte2_rect)

        texte3 = str(Grille[0][1][0][1][0])
        texte3_rendu = police1.render(texte3, True, noir)
        texte3_rect = texte3_rendu.get_rect(center = (fenetre_longueur/100*35, fenetre_hauteur/100*35))
        fenetre.blit(texte3_rendu, texte3_rect)

        texte4 = str(Grille[0][1][1][1][0])
        texte4_rendu = police1.render(texte4, True, noir)
        texte4_rect = texte1_rendu.get_rect(center = (fenetre_longueur/100*85, fenetre_hauteur/100*35))
        fenetre.blit(texte4_rendu, texte4_rect)

        texte5 = str(Grille[1][0][0][1][0])
        texte5_rendu = police1.render(texte5, True, noir)
        texte5_rect = texte5_rendu.get_rect(center = (fenetre_longueur/100*35, fenetre_hauteur/100*60))
        fenetre.blit(texte5_rendu, texte5_rect)

        texte6 = str(Grille[1][0][1][1][0])
        texte6_rendu = police1.render(texte6, True, noir)
        texte6_rect = texte6_rendu.get_rect(center = (fenetre_longueur/100*85, fenetre_hauteur/100*60))
        fenetre.blit(texte6_rendu, texte6_rect)

        texte7 = str(Grille[1][1][0][0][0])
        texte7_rendu = police1.render(texte7, True, noir)
        texte7_rect = texte7_rendu.get_rect(center = (fenetre_longueur/100*10, fenetre_hauteur/100*85))
        fenetre.blit(texte7_rendu, texte7_rect)

        texte8 = str(Grille[1][1][1][0][0])
        texte8_rendu = police1.render(texte8, True, noir)
        texte8_rect = texte8_rendu.get_rect(center = (fenetre_longueur/100*60, fenetre_hauteur/100*85))
        fenetre.blit(texte8_rendu, texte8_rect)

        #Si les cases à compléter on une valeur, on l'affiche
        if int(text1) != 0:
            text11 = str(text1)
            text11_rendu = police1.render(text11, True, blanc)
            text11_rect = text11_rendu.get_rect(center = (fenetre_longueur/100*35, fenetre_hauteur/100*10))
            fenetre.blit(text11_rendu, text11_rect)

        if int(text2) != 0:
            text22 = str(text2)
            text22_rendu = police1.render(text22, True, blanc)
            text22_rect = text22_rendu.get_rect(center = (fenetre_longueur/100*85, fenetre_hauteur/100*10))
            fenetre.blit(text22_rendu, text22_rect)

        if int(text3) != 0:
            text33 = str(text3)
            text33_rendu = police1.render(text33, True, blanc)
            text33_rect = text33_rendu.get_rect(center = (fenetre_longueur/100*10, fenetre_hauteur/100*35))
            fenetre.blit(text33_rendu, text33_rect)

        if int(text4) != 0:
            text44 = str(text4)
            text44_rendu = police1.render(text44, True, blanc)
            text44_rect = text44_rendu.get_rect(center = (fenetre_longueur/100*60, fenetre_hauteur/100*35))
            fenetre.blit(text44_rendu, text44_rect)

        if int(text5) != 0:
            text55 = str(text5)
            text55_rendu = police1.render(text55, True, blanc)
            text55_rect = text55_rendu.get_rect(center = (fenetre_longueur/100*10, fenetre_hauteur/100*60))
            fenetre.blit(text55_rendu, text55_rect)

        if int(text6) != 0:
            text66 = str(text6)
            text66_rendu = police1.render(text66, True, blanc)
            text66_rect = text66_rendu.get_rect(center = (fenetre_longueur/100*35, fenetre_hauteur/100*85))
            fenetre.blit(text66_rendu, text66_rect)

        if int(text7) != 0:
            text77 = str(text7)
            text77_rendu = police1.render(text77, True, blanc)
            text77_rect = text77_rendu.get_rect(center = (fenetre_longueur/100*60, fenetre_hauteur/100*60))
            fenetre.blit(text77_rendu, text77_rect)

        if int(text8) != 0:
            text88 = str(text8)
            text88_rendu = police1.render(text88, True, blanc)
            text88_rect = text88_rendu.get_rect(center = (fenetre_longueur/100*85, fenetre_hauteur/100*85))
            fenetre.blit(text88_rendu, text88_rect)

        pygame.display.update()

        if Grille == Grillev : #Si le sudoku est réussi, on quitte
            bravorect = bravo.get_rect()
            bravorect.center = (window_size[0]/2, window_size[1]/2)
            fenetre.blit(bravo, bravorect)
            pygame.display.update()
            time.sleep(3)
            debut = 0 #Pour stop l'alarme
            continuer = False


        for event in pygame.event.get():

            if event.type == KEYDOWN :
                if event.key == K_ESCAPE: #ECHAP pour quitter le jeu
                    continuer = False
                    debut = 0

            elif event.type == MOUSEBUTTONDOWN and (event.button == 1 or event.button == 2): #Clicker sur une case pour l'augmenter de 1

                if fenetre_longueur/100*25 <= event.pos[0] < fenetre_longueur/100*50 and fenetre_hauteur/100*0 <= event.pos[1] < fenetre_hauteur/100*25:
                    int(text1)
                    text1 += 1
                    if text1 == 5:
                        text1 = 0
                    Grille[0][0][0][1][0] = int(text1)

                if fenetre_longueur/100*75 <= event.pos[0] < fenetre_longueur/100*100 and fenetre_hauteur/100*0 <= event.pos[1] < fenetre_hauteur/100*25:
                    int(text2)
                    text2 += 1
                    if text2 == 5:
                        text2 = 0
                    Grille[0][0][1][1][0] = int(text2)

                if fenetre_longueur/100*0 <= event.pos[0] < fenetre_longueur/100*25 and fenetre_hauteur/100*25 <= event.pos[1] < fenetre_hauteur/100*50:
                    int(text3)
                    text3 += 1
                    if text3 == 5:
                        text3 = 0
                    Grille[0][1][0][0][0] = int(text3)

                if fenetre_longueur/100*50 <= event.pos[0] < fenetre_longueur/100*75 and fenetre_hauteur/100*25 <= event.pos[1] < fenetre_hauteur/100*50:
                    int(text4)
                    text4 += 1
                    if text4 == 5:
                        text4 = 0
                    Grille[0][1][1][0][0] = int(text4)

                if fenetre_longueur/100*0 <= event.pos[0] < fenetre_longueur/100*25 and fenetre_hauteur/100*50 <= event.pos[1] < fenetre_hauteur/100*75:
                    int(text5)
                    text5 += 1
                    if text5 == 5:
                        text5 = 0
                    Grille[1][0][0][0][0] = int(text5)

                if fenetre_longueur/100*25 <= event.pos[0] < fenetre_longueur/100*50 and fenetre_hauteur/100*75 <= event.pos[1] < fenetre_hauteur/100*100:
                    int(text6)
                    text6 += 1
                    if text6 == 5:
                        text6 = 0
                    Grille[1][1][0][1][0] = int(text6)

                if fenetre_longueur/100*50 <= event.pos[0] < fenetre_longueur/100*75 and fenetre_hauteur/100*50 <= event.pos[1] < fenetre_hauteur/100*75:
                    int(text7)
                    text7 += 1
                    if text7 == 5:
                        text7 = 0
                    Grille[1][0][1][0][0] = int(text7)

                if fenetre_longueur/100*75 <= event.pos[0] < fenetre_longueur/100*100 and fenetre_hauteur/100*75 <= event.pos[1] < fenetre_hauteur/100*100:
                    int(text8)
                    text8 += 1
                    if text8 == 5:
                        text8 = 0
                    Grille[1][1][1][1][0] = int(text8)
    return debut

