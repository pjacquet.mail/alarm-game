import sys
import pygame
from pygame.locals import *
from random import randint
from datetime import *
from PIL import Image
import time
from time import thread_time


def reflexe(window_size, debut):

    pygame.init()

    # Définissez la taille de la fenêtre en fonction de la taille de l'écran
    fenetre = pygame.display.set_mode(window_size, pygame.FULLSCREEN)
    fenetre_longueur = window_size[0]
    fenetre_hauteur = window_size[1]

    bravo = Image.open('bravo.jpg')
    bravo_resize = bravo.resize((window_size))
    bravo_resize.save('bravo1.jpg')
    bravo = pygame.image.load('bravo1.jpg')

#---Création de le liste de cases disponibles
    liste_possiblilitee = []

    for i in range (5,95,10):
        for y in range(5,85,10):
            liste_possiblilitee.append([i,y])

    police1 = pygame.font.SysFont("Arial", int(fenetre_longueur/10))
    reussite = 0
    pygame.display.set_caption("Jeu de réflexes !")
    blanc = (255, 255, 255)
    noir = (0,0,0)
    GRAY = (127, 127, 127)
    rouge = (255,0,0)
    bleu = (0,0,255)
    click = True
    running = True

    while running:

        #dessin de la fenetre de base
        fenetre.fill(GRAY)

        #création des cases de la fenêtre a partir d'un pourcentage de l'écran
        for i in range (5,95,10):
            for y in range(5,95,10):
                pygame.draw.rect(fenetre, blanc, pygame.Rect(fenetre_longueur/100*i, fenetre_hauteur/100*y, fenetre_longueur/100*5, fenetre_hauteur/100*5))


        if reussite < 20: #20 cases à clicker

            a = randint(0,71)

            texte1 = str(reussite)
            texte1_rendu = police1.render(texte1, True, blanc)
            texte1_rect = texte1_rendu.get_rect(center = (fenetre_longueur/100*95, fenetre_hauteur/100*10))
            fenetre.blit(texte1_rendu, texte1_rect) #Affichage score

            if click == True : #Affichage de la case rouge
                case_position = liste_possiblilitee[a]
                pygame.draw.rect(fenetre, rouge, pygame.Rect(fenetre_longueur/100*case_position[0], fenetre_hauteur/100*case_position[1], fenetre_longueur/100*5, fenetre_hauteur/100*5))
                pygame.display.update()
                sec = str(datetime.now())[17:19]
                click = False


            for event in pygame.event.get():

                if event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
                    running = False
                    debut = 0 #Quitter le jeu avec ECHAP

                elif event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
                    if not click:

                        if fenetre_longueur/100*case_position[0] <= event.pos[0] <= fenetre_longueur/100*(case_position[0]+5) and fenetre_hauteur/100*case_position[1] <= event.pos[1] <= fenetre_hauteur/100*(case_position[1]+5)  :
                            click = True
                            sec_finale = str(datetime.now())[17:19]
                            temps = int(sec_finale) - int(sec)

                            if temps < 2:
                             reussite += 1 #Si le click est assez rapide on augmente le score
        else :
            bravorect = bravo.get_rect() #Une fois 20 points, on quitte
            bravorect.center = (window_size[0]/2, window_size[1]/2)
            fenetre.blit(bravo, bravorect)
            pygame.display.update()
            time.sleep(3)
            debut = 0
            running = False

    return debut

