# Créé par HENRYG, le 09/02/2024 en Python 3.7
import pygame
from pygame.locals import *
import sys
import datetime
from datetime import *
from pygame import mixer
from PIL import Image
from random import *
import time
from memory import *
from reflexe import *
from calculmental import *
from sudoku import *


pygame.init()
pygame.mixer.init()
pygame.mixer.music.load("alarme.mp3")

# Obtenez la taille de l'écran
fenetre_info = pygame.display.Info()
fenetre_longueur, fenetre_hauteur = fenetre_info.current_w, fenetre_info.current_h

# Définissez la taille de la fenêtre en fonction de la taille de l'écran
window_size = (fenetre_longueur, fenetre_hauteur)
fenetre = pygame.display.set_mode(window_size, pygame.FULLSCREEN)
pygame.display.set_caption("AlarmGame")

#Images pour le fond
para = Image.open('parametre.png')
para_resize = para.resize((int(fenetre_longueur*10/100), int(fenetre_hauteur*10/100)))
para_resize.save('parametre1.png')

param = pygame.image.load('parametre1.png')
pararect = param.get_rect()
pararect.center = (fenetre_longueur*5/100, fenetre_hauteur*5/100)


img = Image.open('city.jpg')
img_resize = img.resize(window_size)
img_resize.save('city1.jpg')

img2 = pygame.image.load('city1.jpg')
imgrect = img2.get_rect()
imgrect.center = (fenetre_longueur/2, fenetre_hauteur/2)


img3 = Image.open('backjeu.jpg')
img3_resize = img3.resize(window_size)
img3_resize.save('backjeu1.jpg')

img4 = pygame.image.load('backjeu1.jpg')
img4rect = img4.get_rect()
img4rect.center = (fenetre_longueur/2, fenetre_hauteur/2)


img5 = Image.open('neige.jpg') #CHANGER L'IMAGE
img5_resize = img5.resize(window_size)
img5_resize.save('neige2.jpg')

img55 = pygame.image.load('neige2.jpg')
img5rect = img55.get_rect()
img5rect.center = (fenetre_longueur/2, fenetre_hauteur/2)


img6 = Image.open('foret.jpg') #CHANGER L'IMAGE
img6_resize = img6.resize(window_size)
img6_resize.save('foret.jpg')

img66 = pygame.image.load('foret.jpg')
img6rect = img66.get_rect()
img6rect.center = (fenetre_longueur/2, fenetre_hauteur/2)


img7 = Image.open('ville.jpg') #CHANGER L'IMAGE
img7_resize = img7.resize(window_size)
img7_resize.save('ville.jpg')

img77 = pygame.image.load('ville.jpg')
img7rect = img77.get_rect()
img7rect.center = (fenetre_longueur/2, fenetre_hauteur/2)

#Images pour les paramètres

choix1 = Image.open('city.jpg')
choix1_resize = choix1.resize((int(fenetre_longueur/100*30), int(fenetre_hauteur/100*40)))
choix1_resize.save('city2.jpg')

choix11 = pygame.image.load('city2.jpg')
choix1rect = choix11.get_rect()
choix1rect.center = (fenetre_longueur/100*25, fenetre_hauteur/100*40)


choix2 = Image.open('neige.jpg') #CHANGER L'IMAGE
choix2_resize = choix2.resize((int(fenetre_longueur/100*30), int(fenetre_hauteur/100*40)))
choix2_resize.save('neige1.jpg')

choix22 = pygame.image.load('neige1.jpg')
choix2rect = choix22.get_rect()
choix2rect.center = (fenetre_longueur/100*75, fenetre_hauteur/100*40)


choix3 = Image.open('foret.jpg') #CHANGER L'IMAGE
choix3_resize = choix3.resize((int(fenetre_longueur/100*30), int(fenetre_hauteur/100*40)))
choix3_resize.save('foret.jpg')

choix33 = pygame.image.load('foret.jpg')
choix3rect = choix33.get_rect()
choix3rect.center = (fenetre_longueur/100*25, fenetre_hauteur/100*80)


choix4 = Image.open('ville.jpg') #CHANGER L'IMAGE
choix4_resize = choix4.resize((int(fenetre_longueur/100*30), int(fenetre_hauteur/100*40)))
choix4_resize.save('ville.jpg')

choix44 = pygame.image.load('ville.jpg')
choix4rect = choix44.get_rect()
choix4rect.center = (fenetre_longueur/100*75, fenetre_hauteur/100*80)


couleur_back = (127, 127, 127)
blanc = (255, 255, 255, 127)
noir = (0,0,0)
rouge = (255,0,0)
vert = (0,255,0)

police1 = pygame.font.SysFont("Arial", int(fenetre_longueur/10))
police2 = pygame.font.SysFont("Arial", int(fenetre_longueur/5))
police3 = pygame.font.SysFont("Arial", int(fenetre_longueur/32))
police4 = pygame.font.SysFont("Arial", int(fenetre_longueur/25))

continuer = True
back = None
choix = None
fond = 1
heures = 0
minutes = 0
txt = ":"
start = "START"
stop = "STOP"
debut = 0 #Si 0 -> on peut selectionner l'heure; si 1 -> on a lancé l'alarme
alarme = 0 #Si 1 -> il faut jouer la musique

def draw_rect_alpha(surface, color, rect):
    '''pour faire une couleur translucide'''
    shape_surf = pygame.Surface(pygame.Rect(rect).size, pygame.SRCALPHA)
    pygame.draw.rect(shape_surf, color, shape_surf.get_rect())
    surface.blit(shape_surf, rect)


while continuer == True:

    mtn = str(datetime.now())[11:16]
    H = int(mtn[0:2]) #que les heures mtn
    M = int(mtn[3:5]) #que les min mtn

    if fond == 1 :
        fenetre.blit(img55, img5rect) #ici pr mettre une image en fond
    elif fond == 2 :
        fenetre.blit(img2, imgrect)
    elif fond == 3 :
        fenetre.blit(img66, img6rect)
    elif fond == 4 :
        fenetre.blit(img77, img7rect)


    fenetre.blit(param, pararect)

    texte1 = "Votre alarme !"
    texte1_rendu = police1.render(texte1, True, blanc)
    texte1_rect = texte1_rendu.get_rect(center = (fenetre_longueur/100*50, fenetre_hauteur/100*10))
    fenetre.blit(texte1_rendu, texte1_rect)

#---Cadran---#
    draw_rect_alpha(fenetre, blanc, pygame.Rect(fenetre_longueur/100*20, fenetre_hauteur/100*40, fenetre_longueur/100*20, fenetre_hauteur/100*30))

    texte2 = str(heures)
    if heures < 10 :
        texte2 = "0" + str(heures)
    texte2_rendu = police2.render(texte2, True, noir)
    texte2_rect = texte2_rendu.get_rect(center = (fenetre_longueur/100*30, fenetre_hauteur/100*55))
    fenetre.blit(texte2_rendu, texte2_rect)


    draw_rect_alpha(fenetre, blanc, pygame.Rect(fenetre_longueur/100*60, fenetre_hauteur/100*40, fenetre_longueur/100*20, fenetre_hauteur/100*30))

    texte3 = str(minutes)
    if minutes < 10 :
        texte3 = "0" + str(minutes)
    texte3_rendu = police2.render(texte3, True, noir)
    texte3_rect = texte3_rendu.get_rect(center = (fenetre_longueur/100*70, fenetre_hauteur/100*55))
    fenetre.blit(texte3_rendu, texte3_rect)

    texte4 = str(txt)
    texte4_rendu = police2.render(texte4, True, noir)
    texte4_rect = texte4_rendu.get_rect(center = (fenetre_longueur/100*50, fenetre_hauteur/100*52.5))
    fenetre.blit(texte4_rendu, texte4_rect)

#---Heure actuelle---#
    texte14 = str(mtn)
    texte14_rendu = police4.render(texte14, True, blanc)
    texte14_rect = texte14_rendu.get_rect(center = (fenetre_longueur/100*94, fenetre_hauteur/100*5))
    fenetre.blit(texte14_rendu, texte14_rect)

#---BOUTON START VISIBLE---#
    if debut == 0 :
        pygame.draw.rect(fenetre, vert, pygame.Rect(fenetre_longueur/100*42, fenetre_hauteur/100*80, fenetre_longueur/100*16, fenetre_hauteur/100*5))

        texte13 = str(start)
        texte13_rendu = police4.render(texte13, True, blanc)
        texte13_rect = texte13_rendu.get_rect(center = (fenetre_longueur/100*50, fenetre_hauteur/100*82.5))
        fenetre.blit(texte13_rendu, texte13_rect)

        pygame.mixer.music.stop()

#---BOUTON STOP VISIBLE---#
    go = 0
    if debut == 1 :
        pygame.draw.rect(fenetre, rouge, pygame.Rect(fenetre_longueur/100*42, fenetre_hauteur/100*80, fenetre_longueur/100*16, fenetre_hauteur/100*5))
        texte14 = str(stop)
        texte14_rendu = police4.render(texte14, True, blanc)
        texte14_rect = texte14_rendu.get_rect(center = (fenetre_longueur/100*50, fenetre_hauteur/100*82.5))
        fenetre.blit(texte14_rendu, texte14_rect)

        pygame.display.update()

        if heures == H and minutes == M :
            go = 1


        if go == 1 :
            go = 2
            jeux = randint(0, 4)
            if jeux == 1: #memory
                pygame.mixer.music.play(-1)
                while jeux == 1:
                    fenetre.blit(img4, img4rect)

                    draw_rect_alpha(fenetre, blanc, pygame.Rect(fenetre_longueur/100*72.5, fenetre_hauteur/100*80, fenetre_longueur/100*15, fenetre_hauteur/100*10))
                    play = "Jouer"
                    play_rendu = police4.render(play, True, noir)
                    play_rect = play_rendu.get_rect(center = (fenetre_longueur/100*80, fenetre_hauteur/100*85))
                    fenetre.blit(play_rendu, play_rect)

                    memo = "Mémory"
                    memo_rendu = police1.render(memo, True, blanc)
                    memo_rect = memo_rendu.get_rect(center = (fenetre_longueur/100*32.5, fenetre_hauteur/100*45))
                    fenetre.blit(memo_rendu, memo_rect)

                    pygame.display.update()

                    for event in pygame.event.get():
                        if event.type == KEYDOWN:
                            if event.key == K_ESCAPE:
                                debut = 0
                                jeux = None
                        elif event.type == pygame.MOUSEBUTTONDOWN:
                            if fenetre_longueur/100*72.5 <= event.pos[0] <= fenetre_longueur/100*87.5 and fenetre_hauteur/100*80 <= event.pos[1] <= fenetre_hauteur/100*90 :
                                jeux = None
                                debut = memory(window_size, debut)
                                go = 2


            elif jeux == 2: #Rapidité
                pygame.mixer.music.play(-1)
                while jeux == 2:
                    fenetre.blit(img4, img4rect)

                    draw_rect_alpha(fenetre, blanc, pygame.Rect(fenetre_longueur/100*72.5, fenetre_hauteur/100*80, fenetre_longueur/100*15, fenetre_hauteur/100*10))
                    play = "Jouer"
                    play_rendu = police4.render(play, True, noir)
                    play_rect = play_rendu.get_rect(center = (fenetre_longueur/100*80, fenetre_hauteur/100*85))
                    fenetre.blit(play_rendu, play_rect)

                    rflx = "Reflexe"
                    rflx_rendu = police1.render(rflx, True, blanc)
                    rflx_rect = rflx_rendu.get_rect(center = (fenetre_longueur/100*32.5, fenetre_hauteur/100*45))
                    fenetre.blit(rflx_rendu, rflx_rect)

                    pygame.display.update()

                    for event in pygame.event.get():
                        if event.type == KEYDOWN:
                            if event.key == K_ESCAPE:
                                debut = 0
                                jeux = None
                        elif event.type == pygame.MOUSEBUTTONDOWN:
                            if fenetre_longueur/100*72.5 <= event.pos[0] <= fenetre_longueur/100*87.5 and fenetre_hauteur/100*80 <= event.pos[1] <= fenetre_hauteur/100*90 :
                                jeux = None
                                debut = reflexe(window_size, debut)
                                go = 2


            elif jeux == 3: #Calcul
                pygame.mixer.music.play(-1)
                while jeux == 3 :
                    fenetre.blit(img4, img4rect)


                    draw_rect_alpha(fenetre, blanc, pygame.Rect(fenetre_longueur/100*72.5, fenetre_hauteur/100*80, fenetre_longueur/100*15, fenetre_hauteur/100*10))
                    play = "Jouer"
                    play_rendu = police4.render(play, True, noir)
                    play_rect = play_rendu.get_rect(center = (fenetre_longueur/100*80, fenetre_hauteur/100*85))
                    fenetre.blit(play_rendu, play_rect)

                    calc = "Calcul mental"
                    calc_rendu = police1.render(calc, True, blanc)
                    calc_rect = calc_rendu.get_rect(center = (fenetre_longueur/100*32.5, fenetre_hauteur/100*45))
                    fenetre.blit(calc_rendu, calc_rect)

                    pygame.display.update()

                    for event in pygame.event.get():
                        if event.type == KEYDOWN:
                            if event.key == K_ESCAPE:
                                debut = 0
                                jeux = None
                        elif event.type == pygame.MOUSEBUTTONDOWN:
                            if fenetre_longueur/100*72.5 <= event.pos[0] <= fenetre_longueur/100*87.5 and fenetre_hauteur/100*80 <= event.pos[1] <= fenetre_hauteur/100*90 :
                                jeux = None
                                debut = exec(window_size, debut)
                                go = 2

            elif jeux == 4: #Sudoku
                pygame.mixer.music.play(-1)
                while jeux == 4 :
                    fenetre.blit(img4, img4rect)

                    draw_rect_alpha(fenetre, blanc, pygame.Rect(fenetre_longueur/100*72.5, fenetre_hauteur/100*80, fenetre_longueur/100*15, fenetre_hauteur/100*10))
                    play = "Jouer"
                    play_rendu = police4.render(play, True, noir)
                    play_rect = play_rendu.get_rect(center = (fenetre_longueur/100*80, fenetre_hauteur/100*85))
                    fenetre.blit(play_rendu, play_rect)

                    sudok = "Sudoku"
                    sudok_rendu = police1.render(sudok, True, blanc)
                    sudok_rect = sudok_rendu.get_rect(center = (fenetre_longueur/100*32.5, fenetre_hauteur/100*45))
                    fenetre.blit(sudok_rendu, sudok_rect)

                    pygame.display.update()

                    for event in pygame.event.get():
                        if event.type == KEYDOWN:
                            if event.key == K_ESCAPE:
                                debut = 0
                                jeux = None
                        elif event.type == pygame.MOUSEBUTTONDOWN:
                            if fenetre_longueur/100*72.5 <= event.pos[0] <= fenetre_longueur/100*87.5 and fenetre_hauteur/100*80 <= event.pos[1] <= fenetre_hauteur/100*90 :
                                jeux = None
                                debut = sudoku(window_size, debut)
                                go = 2
    pygame.display.update()

    for event in pygame.event.get():
        if event.type == pygame.MOUSEBUTTONDOWN: #Voir si possibilité des flèches en plus car pas pratique sur pc portable
            if debut == 0 : #Choix heures et minutes
                if fenetre_longueur/100*20 <= event.pos[0] <= fenetre_longueur/100*40 and fenetre_hauteur/100*40 <= event.pos[1] <= fenetre_hauteur/100*70 :
                    if event.button == 4 :
                        heures += 1
                        if heures >= 24 :
                            heures = 0
                    elif event.button == 5 :
                        heures -= 1
                        if heures <= 0 :
                            heures = 23

                elif fenetre_longueur/100*0 <= event.pos[0] <= fenetre_longueur/100*10 and fenetre_hauteur/100*0 <= event.pos[1] <= fenetre_hauteur/100*10 :
                    choix = 0
                    while choix == 0 :
                        fenetre.fill(blanc)

                        back = "Choix du fond"
                        back_rendu = police1.render(back, True, noir)
                        back_rect = back_rendu.get_rect(center = (fenetre_longueur/100*50, fenetre_hauteur/100*10))
                        fenetre.blit(back_rendu, back_rect)

                        fenetre.blit(choix11, choix1rect)
                        fenetre.blit(choix22, choix2rect)
                        fenetre.blit(choix33, choix3rect)
                        fenetre.blit(choix44, choix4rect)

                        pygame.draw.rect(fenetre, noir, pygame.Rect(fenetre_longueur/100*0, fenetre_hauteur/100*20, fenetre_longueur/100*100, fenetre_hauteur/100*1))
                        pygame.draw.rect(fenetre, noir, pygame.Rect(fenetre_longueur/100*50, fenetre_hauteur/100*20, fenetre_longueur/100*1, fenetre_hauteur/100*80))
                        pygame.draw.rect(fenetre, noir, pygame.Rect(fenetre_longueur/100*0, fenetre_hauteur/100*60, fenetre_longueur/100*100, fenetre_hauteur/100*1))

                        pygame.display.update()

                        for event in pygame.event.get():
                            if event.type == MOUSEBUTTONDOWN:

                                if fenetre_longueur/100*50 < event.pos[0] < fenetre_longueur and fenetre_hauteur/100*20 < event.pos[1] < fenetre_hauteur/100*60 :
                                    fond = 1
                                    choix = 1

                                if fenetre_longueur/100*0 < event.pos[0] < fenetre_longueur/100*50 and fenetre_hauteur/100*20 < event.pos[1] < fenetre_hauteur/100*60 :
                                    fond = 2
                                    choix = 1

                                elif fenetre_longueur/100*0 < event.pos[0] < fenetre_longueur/100*50 and fenetre_hauteur/100*60 < event.pos[1] < fenetre_hauteur:
                                    fond = 3
                                    choix = 1

                                elif fenetre_longueur/100*50 < event.pos[0] < fenetre_longueur and fenetre_hauteur/100*60 < event.pos[1] < fenetre_hauteur:
                                    fond = 4
                                    choix = 1

                elif fenetre_longueur/100*60 <= event.pos[0] <= fenetre_longueur/100*80 and fenetre_hauteur/100*40 <= event.pos[1] <= fenetre_hauteur/100*70 :
                    if event.button == 4 :
                        minutes += 1
                        if minutes >= 60 :
                            minutes = 0
                    elif event.button == 5 :
                        minutes -= 1
                        if minutes <= 0 :
                            minutes = 59


            if fenetre_longueur/100*42 <= event.pos[0] <= fenetre_longueur/100*58 and fenetre_hauteur/100*80 <= event.pos[1] <= fenetre_hauteur/100*85 :
                if debut == 0 : #Si stop visible on passe au start et inversement
                    debut = 1
                else :
                    debut = 0

        elif event.type == KEYDOWN:
            if event.key == K_ESCAPE:
                continuer = False

pygame.quit()